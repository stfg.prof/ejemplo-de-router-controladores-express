const lista=require("../data/listaDeObjetos.js");

const controlador={
    index: (req, res) => {
        res.render('pages/index')},

    gracias:(req,res)=>{
        user = {
            nombre:req.query.nombre || "nombre desconocido",
            apellido:req.query.apellido ||"apellido desconocido",
            login:true
        }

        res.render("pages/gracias",{user})
    },
    formulario:(req,res)=>{
        res.render("pages/formulario")
    },
    catalogo:(req,res)=>{
        res.render("pages/catalogo",{lista})
    }

}

module.exports = controlador;