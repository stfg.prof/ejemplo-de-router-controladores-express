
//importo la libreria express que sera nuestro servidor
const express = require('express')

//importamos nuesttras rutas
const router=require("./routes/usuario.js")

//llamo a la funcion express que nos devuelve una aplicacion de servidor 
const app = express();
//defino el puerto (esto suele hacerse con variables de entorno, para usos practicos lo hacemos asignadolo a una constante)
const port = 3000;

//defino el motor de renderizado de plantillas
app.set('view engine', 'ejs');


//defino en el servidor una carpeta estatica que sera expuesta (podemos leer el contenido desde la web) 
app.use(express.static('public'));

//le asignamos el objeto router al servidor
app.use(router);


//lanzo el servidor, a un puerto determinado 
app.listen(port, () => {
    //emito un msj a la consola con el puerto,
  console.log(`http://localhost:${port}`)
})