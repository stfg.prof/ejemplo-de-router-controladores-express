//importamos el modulo expres 
const express = require('express')
const controlador=require("../controllers/controlador.js")

//del modulo express tomamos el objeto router
const router = express.Router();


router.get('/',controlador.index);

router.get('/formulario', controlador.formulario);

router.get('/catalogo', controlador.catalogo);

router.get('/gracias', controlador.gracias);

module.exports = router;